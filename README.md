Site content edit instruction
=============================

## File structure ##

	`
    |-- albums          # gallery folder
	|-- assets			# resources folder
	|	|-- font		# fonts folder
	|	|-- img			# images folder
	|	|	|-- css		# images used by css, background etc.
	|	|	|-- photo	# photos placed here
	|	|-- js			# javascript files
    |-- plugins         # additional function using plugins
	|-- .htaccess		# apache URL config file, DONT DELETE!
	`-- index.html		# main page file

## General ##

All pages are designed in only one file `index.html` to provide smooth animation between pages change.

The content of each page are designed as template wrapped with `<script type="text/template">` tag. It can contain any HTML content except `<script>` tag.

And all CSS are in `index.html` too, to avoid an additional HTTP request.

Main navigation in each page is in line 172 (by `id="template-nav"`).

## Home page ##

### Navigation ###

In `index.html` line 350 (Find `site.home.color`), here config the arborjs circles which contain title of the circle, color, and link.

And text on left side you can find in line 209 (by `id="template-home"`).

## About page ##

All about text you can edit in line 236 (in `<div class="text">`).

## Photo page ##

Photo gallery is very easy to config.

0. First, throw all your photos categorized as each folder in to `/albums` folder.
0. Select one picture file in each album folder which you want it to be the cover of the album, and add an additional `!` to file name as the first character.
0. Make sure the right of cache folder `/plugins/gallery/cache` is `777` on your server.

That's all! Then when you visit site first time, all the thumbnail picture will be automately generated.

## Other page ##

In "contact" (address) page and "links" page they are just same as "about" page. Modify the content from line 287 (by `id="template-contact"`) and line 326 (by `id="template-links"`) in `*-block` class defined tag. You can't miss them!
