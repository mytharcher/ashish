<?php
require_once 'UberGallery.php';

$albumsPath = $_SERVER['DOCUMENT_ROOT'] . $_GET['path'];
$result = array();

$albums = scandir($albumsPath);
foreach ($albums as $album) {
	$dir = "$albumsPath/$album";
	if (is_dir($dir) && strpos($album, '.') !== 0) {
		$photos = scandir($dir);
		$amount = count($photos) - 2;
		if ($amount) {
			$len = count($result);
			$gallery = UberGallery::init()->readImageDirectory($dir);
			$name = $album;
			$result[] = array(
				'title' => ucwords($name),
				'path' => $album,
				'photos' => $gallery
			);
		}
	}
}

echo json_encode($result);
?>