
var site = {
	lib: {
		template: function (tpl, data) {
			return Mustache.render(
				$('#template-' + tpl).html()
				.replace(/\{\{>([^\}]+)\}\}/g, function (matcher, name) {
					return $('#template-' + name).html();
				}), data);
		}
	},

	home: {
		Renderer: function(){
			var dom = $("#sitemap");
			var canvas = dom.get(0);
			
			var ctx = canvas.getContext("2d");
			var gfx = arbor.Graphics(canvas);
			var sys = null;

			var _vignette = null;
			var selected = null,
					nearest = null,
					_mouseP = null;

			
			var that = {
				init:function(pSystem){
					sys = pSystem;
					sys.screen({size:{width:dom.width(), height:dom.height()},
											padding:[36,60,36,60]});

					$(window).resize(that.resize);
					that.resize();
					that._initMouseHandling();
				},
				resize:function(){
					canvas.width = Math.max($(window).width() - 270, 800);
					canvas.height = $(window).height();
					sys.screen({size:{width:canvas.width, height:canvas.height}});
					_vignette = null;
					that.redraw();
				},
				redraw:function(){
					gfx.clear();
					sys.eachEdge(function(edge, p1, p2){
						if (edge.source.data.alpha * edge.target.data.alpha === 0) {
							return;
						}
						gfx.line(p1, p2, {stroke:"#b2b19d", width:2, alpha:edge.target.data.alpha});
					});
					sys.eachNode(function(node, pt){
						var w = Math.max(20, 20+gfx.textWidth(node.name) );
						if (node.data.alpha===0) {
							return;
						}
						if (node.data.shape=='dot'){
							gfx.oval(pt.x-w/2, pt.y-w/2, w, w, {fill:node.data.color, alpha:node.data.alpha});
							gfx.text(node.name, pt.x, pt.y+7, {color:"white", align:"center", font:"Arial", size:12});
							gfx.text(node.name, pt.x, pt.y+7, {color:"white", align:"center", font:"Arial", size:12});
						}else{
							gfx.rect(pt.x-w/2, pt.y-8, w, 20, 4, {fill:node.data.color, alpha:node.data.alpha});
							gfx.text(node.name, pt.x, pt.y+9, {color:"white", align:"center", font:"Arial", size:12});
							gfx.text(node.name, pt.x, pt.y+9, {color:"white", align:"center", font:"Arial", size:12});
						}
					});
				},

				switchMode:function(e){
					if (e.mode=='hidden'){
						dom.stop(true).fadeTo(e.dt,0, function(){
							if (sys) sys.stop();
							$(this).hide();
						});
					}else if (e.mode=='visible'){
						dom.stop(true).css('opacity',0).show().fadeTo(e.dt,1,function(){
							that.resize();
						});
						if (sys) {
							sys.start();
						}
					}
				},
				
				switchSection:function(newSection){
					var childrenEdges = sys.getEdgesFrom(newSection);
					if (!childrenEdges.length) {
						return;
					}
					var parent = sys.getEdgesFrom(newSection)[0].source;
					var children = $.map(sys.getEdgesFrom(newSection), function(edge){
						return edge.target;
					});
					
					sys.eachNode(function(node){
						if (node.data.shape=='dot') {
							return; // skip all but leafnodes
						}

						var nowVisible = ($.inArray(node, children)>=0);
						var newAlpha = (nowVisible) ? 1 : 0;
						var dt = (nowVisible) ? 0.5 : 0.5;
						sys.tweenNode(node, dt, {alpha:newAlpha});

						if (newAlpha==1){
							node.p.x = parent.p.x + 0.05*Math.random() - 0.025;
							node.p.y = parent.p.y + 0.05*Math.random() - 0.025;
							node.tempMass = 0.001;
						}
					});
				},
				
				
				_initMouseHandling:function(){
					// no-nonsense drag and drop (thanks springy.js)
					selected = null;
					nearest = null;
					var dragged = null;
					var oldmass = 1;

					var _section = null;

					var handler = {
						moved:function(e){
							var pos = $(canvas).offset();
							_mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
							nearest = sys.nearest(_mouseP);

							if (!nearest.node) {
								return false;
							}

							if (nearest.node.data.hasChild){
								if (nearest.node.name!=_section){
									_section = nearest.node.name;
									that.switchSection(_section);
								}
							}
							
							selected = (nearest.distance < 50) ? nearest : null;
							
							return false;
						},
						pressed:function(e){
							var pos = $(canvas).offset();
							_mouseP = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);
							nearest = sys.nearest(_mouseP);
							
							if (nearest && selected && nearest.node===selected.node){
								var link = selected.node.data.link;
								if (link) {
									if (link.match(/^https?\:\/\//)){
										window.location.href = link;
									}else{
										if (history.pushState) {
											Nav.route(link);
										} else {
											location.hash = link;
										}
									}
								} else {
									$(canvas).unbind('mousemove', handler.moved);
									$(canvas).bind('mousemove', handler.dragged);
								}
							}

							return false;
						},
						dragged:function(e){
							dragged = nearest;
							
							if (dragged && dragged.node !== null) {
								dragged.node.fixed = true;
							}
							
							var old_nearest = nearest && nearest.node._id;
							var pos = $(canvas).offset();
							var s = arbor.Point(e.pageX-pos.left, e.pageY-pos.top);

							if (!nearest) return;
							if (dragged !== null && dragged.node !== null){
								var p = sys.fromScreen(s);
								dragged.node.p = p;
							}
							
							$(window).bind('mouseup', handler.dropped);

							return false;
						},

						dropped:function(e){
							if (dragged===null || dragged.node===undefined) {
								return;
							}
							if (dragged.node !== null) {
								dragged.node.fixed = false;
							}
							dragged.node.tempMass = 1000;
							dragged = null;
							// selected = null
							$(canvas).unbind('mousemove', handler.dragged);
							$(window).unbind('mouseup', handler.dropped);
							$(canvas).bind('mousemove', handler.moved);
							_mouseP = null;
							return false;
						}


					};

					$(canvas).mousedown(handler.pressed);
					$(canvas).mousemove(handler.moved);

				}
			};
			
			return that;
		},
		
		init: function(){
			if ($.browser.msie) {
				$('#main').html($('#template-nav').html());
			} else {
				var sys = arbor.ParticleSystem();
				sys.parameters({stiffness:900, repulsion:2000, gravity:true, dt:0.015});
				sys.renderer = site.home.Renderer();
				sys.graft(site.home.ui);
			}
		},
		
		dispose: function () {
			$("#sitemap").unbind();
		}
	}, // end home
	
	about: {},
	
	photo: {
		init: function (sub) {
			var me = site.photo;
			if (me.data) {
				me.update(sub);
			} else {
				$.ajax(me.dataUrl, {
					data: {path: me.albumsPath || 'albums'},
					dataType: 'json',
					success: function (data) {
						me.data = data;
						me.update(sub);
					}
				});
			}
		},
		update: function (sub) {
			var me = site.photo;
			var html;
			if (sub) {
				var data = me.data.filter(function (item) {
					return item.path == sub;
				});
				if (data.length) {
					html = site.lib.template('photo-album', data[0]);
				} else {
					Nav.route('/photo');
				}
				$('#body a[rel=fancybox]').fancybox();
				$('#body').on('click', 'h3 a', me.onredirect);
			} else {
				html = site.lib.template('photo-albums', me.data);
				$('#body').on('click', '.photo-albums a', me.onredirect);
			}
			$('#body').html(html);
		},
		dispose: function () {
			$('#body').unbind();
		},

		onredirect: function (ev) {
			Nav.route(this.href);
			return false;
		},
		
		oncontextmenu: function (ev) {
			ev && ev.preventDefault();
			return false;
		}
	},
	
	contact: {},
	
	links: {
		init: function () {
			var me = site.links;
			if (!me.content) {
				$.get(me.dataUrl || '/links.md', function (data) {
					console.log(data);
					me.content = data;
					me.update();
				});
			} else {
				me.update();
			}
		},
		update: function () {
			// console.log(html);
			var converter = new Showdown.converter();
			var html = converter.makeHtml(this.content);
			$('#body div.links-block').html(html);
		}
	}
};

var Nav = {
	route: function (path, force) {
		path = (path || '/').replace(location.protocol + '//' + location.host, '');
		var isNew = path != location.pathname;
		if (isNew){
			Nav.record(path);
		}
		if (isNew || force) {
			Nav.update(path);
		}
	},
	
	record: function (path) {
		var useHistory = history.pushState;
		if (useHistory) {
			history.pushState({path:path}, "", path);
		} else {
			var iframe = document.getElementById('IEHistory'),
			iframeDoc = iframe.contentWindow.document;

			iframeDoc.open( 'text/html' );
			iframeDoc.write('<html><head></head><body><input type="text" id="save" value="' + path + '"></body></html>');
			iframeDoc.close();
		}
	},
	
	update: function (path) {
		var pathname = path.replace(/^#?\/|\/$/g, '') || 'home';
		var paths = pathname.split('/');
		var subSlashIndex = pathname.indexOf('/');
		var subPath = subSlashIndex >= 0 ? pathname.slice(subSlashIndex + 1) : '';
		var mainPath = paths[0];
		var action = site[mainPath];
		if (action) {
			if (Nav.current != action) {
				$(document.body).removeClass('loaded');
				setTimeout(function () {
					if (Nav.current) {
						Nav.current.dispose && Nav.current.dispose();
						$('#nav a').unbind();
					}
					Nav.current = action;
					Nav.template(mainPath);
					Nav.current.init && Nav.current.init(subPath);
					$('#nav a').click(Nav.clickHander);
					setTimeout(function () {
						Nav.highlight(path);
						$(document.body).addClass('loaded');
					}, 150);
					// setTimeout(action.init, 500);
						
				}, 500);
				// Nav.enter(action.init);
			} else {
				action.dispose && action.dispose();
				action.init && action.init(subPath);
			}
		} else {
			throw new Error('404. Page of this path not found.');
		}
	},
	
	template: function (pathname) {
		$('#holder').html(site.lib.template(pathname));
	},
	
	highlight: function (path) {
		var nav = $('#nav>ul>li');
		var cur;
		nav.each(function (index, li) {
			var item = $(li);
			item.removeClass('active');
			var href = item.find('a').attr('href');
			// console.log(path);
			if (href == path) {
				cur = item;
			}
		});
		if (cur) {
			cur.addClass('active');
		}
	},
	
	clickHander: function () {
		var path = $(this).attr('href');
		if (!path.match(/^https?\:\/\//)) {
			if (history.pushState) {
				Nav.route(path);
			} else {
				location.hash = '#' + path;
			}
			return false;
		}
	},
	
	onchange: function (ev) {
		Nav.update(history.pushState ?
			location.pathname :
			location.hash);
	}
};


$(document).ready(function () {
	if (history.pushState) {
		Nav.route(location.pathname, true);
		window.onpopstate = Nav.onchange;
	} else {
		if (location.pathname && !location.hash) {
			location.href = '/#' + location.pathname;
		} else {
			Nav.route(location.hash, true);
		}
		window.onhashchange = Nav.onchange;
	}
});
